# PULSE

The project was created with:
+ preprocessor scss;
+ library jQuery (https://jquery.com/);
+ PHPMailer (https://github.com/PHPMailer/PHPMailer);
+ methodology BEM (https://ru.bem.info/methodology/).

![Project screenshot](https://gitflic.ru/project/msi/pulse/blob/?file=screenshot.jpg&branch=master)
